import dotenv from 'dotenv';
dotenv.config();

import bodyParser from 'body-parser';
import express, { Response, NextFunction } from 'express';
import path from 'path';
import { NotFoundError } from './errors/not-found-error';
import { appRoutes } from './router';
import { logger } from './utils/logger';
import { IAppError } from './types/errors';

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(appRoutes);

app.listen(port, () => logger.info(`App listening on port ${port}!`));

// Catch 404 and forward to error handler
app.use((req, _res, next) => {
  const err = new NotFoundError(`Not Found: ${req.url}`);
  next(err);
});

// Will print stacktrace in development
const errorMessage = (err: IAppError) => (app.get('env') === 'development' ? err.message : '');

// Error handlers
app.use((err: IAppError, _req: any, res: Response, next: NextFunction) => {
  if (res.headersSent) {
    next(err);
    return;
  }

  logger.error(err.message);

  if (err.status === 404) {
    res.render('404');
    return;
  }

  res.status(err.status || 500);
  res.render('error', {
    message: errorMessage(err)
  });
});
