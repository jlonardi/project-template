import path from 'path';
import express from 'express';
import morgan from 'morgan';
import { morganLogFormatter } from './utils/formatter';
import { publicRoutes } from './routes/public.routes';

const router = express.Router();

router.use(express.static(path.join(__dirname, 'static')));
router.use(morgan(morganLogFormatter)); // place below static files to avoid static file request logging
router.use(publicRoutes);

export const appRoutes = router;
