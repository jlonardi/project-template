#!/bin/sh

$(aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 336002603937.dkr.ecr.eu-west-1.amazonaws.com/networked-systems-project)

docker build -t node-app ./
docker tag node-app 336002603937.dkr.ecr.eu-west-1.amazonaws.com/networked-systems-project:$CI_PIPELINE_ID
docker push 336002603937.dkr.ecr.eu-west-1.amazonaws.com/networked-systems-project:$CI_PIPELINE_ID
docker tag node-app 336002603937.dkr.ecr.eu-west-1.amazonaws.com/networked-systems-project:latest
docker push 336002603937.dkr.ecr.eu-west-1.amazonaws.com/networked-systems-project:latest
