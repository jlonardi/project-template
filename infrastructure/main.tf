terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  backend "s3" {
    profile = "networked-systems-project"
    bucket = "networking-project-terraform-state-bucket"
    key    = "state/terraform_state.tfstate"
    region = "eu-west-1"
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "networked-systems-project"
  region  = "eu-west-1"
}


