# ecs.tf | Elastic Container Service Configuration

resource "aws_ecs_cluster" "ecs-cluster" {
  name = "fargate-cluster"
}

resource "aws_cloudwatch_log_group" "log-group" {
  name = "node-app-logs"
}

resource "aws_ecs_task_definition" "app_definition" {
  family = "node-app"
  container_definitions = jsonencode([
    {
      name      = "node-app-container"
      image     = "${aws_ecr_repository.ecr_repository.repository_url}"
      essential = true
      logConfiguration: {
        logDriver: "awslogs"
        options: {
          "awslogs-group": "${aws_cloudwatch_log_group.log-group.id}",
          "awslogs-region": "eu-west-1",
          "awslogs-stream-prefix": "node-app"
        }
      }
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
    }
  ])
  network_mode = "awsvpc"
  cpu       = 256
  memory    = 512
  requires_compatibilities = ["FARGATE"]
  execution_role_arn = "arn:aws:iam::336002603937:role/ecsTaskExecutionRole"
}


resource "aws_ecs_service" "aws-ecs-service" {
  name            = "node-app-ecs-service"
  cluster         = aws_ecs_cluster.ecs-cluster.id
  task_definition = aws_ecs_task_definition.app_definition.arn
  desired_count   = 3
  launch_type          = "FARGATE"
  scheduling_strategy  = "REPLICA"
  force_new_deployment = true

  network_configuration {
    subnets          = aws_subnet.subnets.*.id
    assign_public_ip = true
    security_groups = [
      aws_security_group.service_security_group.id,
    ]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.target_group.arn
    container_name   = "node-app-container"
    container_port   = 80
  }

  depends_on = [aws_lb_listener.listener]
}

resource "aws_security_group" "service_security_group" {
  vpc_id = aws_vpc.aws-vpc.id

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "node-app-service-sg"
  }
}
