# variables.tf | Auth and Application variables

# variable "aws_access_key" {
#   type        = string
#   description = "AWS Access Key"
# }

# variable "aws_secret_key" {
#   type        = string
#   description = "AWS Secret Key"
# }

# variable "aws_key_pair_name" {
#   type        = string
#   description = "AWS Key Pair Name"
# }

# variable "aws_key_pair_file" {
#   type = string
#   description = "AWS Key Pair File"
# }

variable "aws_region" {
  type        = string
  description = "AWS Region"
}

variable "cidr" {
  description = "The CIDR block for the VPC."
  default     = "10.0.0.0/16"
}

variable "subnets" {
  description = "List of private subnets"
}

variable "availability_zones" {
  description = "List of availability zones"
}

# variable "database_name" {
#   description = "Database Name"
# }

# variable "database_password" {
#   description = "Database Password"
# }
