# networking.tf | Virtual Private Cloud Configuration

resource "aws_vpc" "aws-vpc" {
  cidr_block         = "10.10.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name        = "node-app-vpc"
  }
}

resource "aws_subnet" "subnets" {
  vpc_id                  = aws_vpc.aws-vpc.id
  map_public_ip_on_launch = true
  count                   = length(var.subnets)
  cidr_block              = element(var.subnets, count.index)
  availability_zone       = element(var.availability_zones, count.index)
}

resource "aws_internet_gateway" "aws-igw" {
  vpc_id = aws_vpc.aws-vpc.id
  tags = {
    Name        = "node-app-igw"
  }
}

resource "aws_main_route_table_association" "a" {
  vpc_id         = aws_vpc.aws-vpc.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.aws-vpc.id

  tags = {
    Name        = "node-app-routing-table-public"
  }
}

resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.aws-igw.id
}

resource "aws_route_table_association" "public" {
  count          = length(var.subnets)
  subnet_id      = element(aws_subnet.subnets.*.id, count.index)
  route_table_id = aws_route_table.public.id
}
