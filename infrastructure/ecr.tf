# ecr.tf | Elastic Container Registry Configuration

resource "aws_ecr_repository" "ecr_repository" {
  name                 = "networked-systems-project"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }
}