# AWS deployment pipeline

This project includes a simple node application, a GitLab CI/CD pipeline and a AWS-infrastructure configred with Terrafrom.

## Development

The following are the instrocution to make the run the appication on your local machine

### Application

To run the application in watch-mode for development

```bash
$ npm run dev
```

and go to http://localhost:3000

## Building

To build the project for example for deployment

```bash
$ npm run build
```

The application can be run inside a docker container. The containers are build using `Dockerfile`. To run the app with docker

```bash
$ docker build -t node-app .
$ docker run --publish 3000:80 node-app
```

and the app is again available from http://localhost:3000

## AWS and Terraform

For running terraform you need to have an AWS account and to create an IAM user in order to obtain an access key and the secrect access key for proggamatically run code. Make sure to give the IAM user enough permission to run all the services described in the `.tf` files. For testing purposes it is possible to just give full administrative access for the user, but this is not recommended.

To run Terraform locally you need to install the corresponding AWS-CLI for your operating system and run `aws configure` where you enter the earlier obtained access keys.

```bash
$ aws configure
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: us-west-2
Default output format [None]: json
```

After this navigate in the `./terraform` directory and run

```bash
$ terraform init
$ terraform plan
$ terraform apply
```

After confirming the apply with a `yes` Terraform will update the infrastructure on your AWS-account.

To push your earlier build docker container you need to login to your ECR and pass it to docker. You need to replace `{account}` wiuth your account number.

```bash
$ aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin {account}.dkr.ecr.eu-west-1.amazonaws.com/networked-systems-project
```

then tag the container and push

```bash
$ docker tag node-app {account}.dkr.ecr.eu-west-1.amazonaws.com/networked-systems-project
$ docker push {account}.dkr.ecr.eu-west-1.amazonaws.com/networked-systems-project
```

After a while your container should be running in AWS.

To bring the infrastructure down run

```bash
$ terraform destroy
```

This might take a while since the ECS service and internet gateway can't be removed before the containers and resources are drained.

## CI/CD

The GitLab CI/CD pipeline is the standard .gitlab-ci.yml file. To make the pipeline work you need to edit the `./scripts/publish-images.sh` file and replace the docker repository with your own as these urls are bound to certain aws-account number.

You also need to go in your gitlab CI/CD settings and add the following environment variables so the AWS CLI can work.

| Env. variable name      | Value                  |
| ----------------------- | ---------------------- |
| `AWS_ACCESS_KEY_ID`     | Your Access key ID     |
| `AWS_SECRET_ACCESS_KEY` | Your Secret access key |
| `AWS_DEFAULT_REGION`    | Your region code       |
